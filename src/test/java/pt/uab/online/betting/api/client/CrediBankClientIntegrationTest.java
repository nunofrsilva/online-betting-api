package pt.uab.online.betting.api.client;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pt.uab.online.betting.api.domain.DigitalCheck;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
public class CrediBankClientIntegrationTest {

    @Autowired
    public CrediBankClient client;

    @Test
    public void digitalCheck_thanReturnDigitalCheck() {
        DigitalCheck digitalCheck = client.digitalCheck(12345678, 10.0);
        assertFalse(digitalCheck.getCheckID().isEmpty());
        assertFalse(digitalCheck.getDate().toString().isEmpty());
    }
}
