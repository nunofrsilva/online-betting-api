package pt.uab.online.betting.api.dto;

import lombok.Data;

@Data
public class CrediBankRequest {

    private int accountId;

    private Double value;
}
