package pt.uab.online.betting.api.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import pt.uab.online.betting.api.client.CrediBankClient;
import pt.uab.online.betting.api.domain.DigitalCheck;
import pt.uab.online.betting.api.dto.CrediBankRequest;

import javax.validation.Valid;

@RestController
@Tag(name = "Sistema CrediBank", description = "É um sistema de pagamento online, que através de uma API Webservice REST " +
        "permite obter um cheque digital que pode ser usado para pagamentos noutro serviço eletrónico.")
public class CrediBankController {

    @Autowired
    private CrediBankClient serviceClient;

    @PostMapping("/credibank")
    private DigitalCheck getDigitalCheck(@RequestBody CrediBankRequest request) {
        if (String.valueOf(request.getAccountId()).length() == 8 && request.getValue() > 0) {
            return serviceClient.digitalCheck(request.getAccountId(), request.getValue());
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Verifique se o 'accountId' tem 8 digitos e se o 'valor' é superior a 0");
    }
}
