package pt.uab.online.betting.api.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import pt.uab.online.betting.api.domain.DigitalCheck;

/**
 * Cliente utilizado para comunicar com o serviço CrediBank
 */
@Component
public class CrediBankClient {

    // Caminho para a relização do pedido
    private static String FETCH_DIGITAL_CHECK = "/check/{accountId}/amount/{value}";

    // Cliente HTTP configurado com o BaseUrl do servilo CrediBank
    @Autowired
    @Qualifier("credibankWebClient")
    private WebClient webClient;

    /**
     * Função utilizada para chamar o serviço e retornar um objeto DigitalCheck
     * com os dados de resposta ao serviço CrediBank
     * @param accountId
     * @param value
     * @return
     */
    public DigitalCheck digitalCheck(final int accountId, final Double value) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder.path(FETCH_DIGITAL_CHECK).build(accountId, value))
                .retrieve()
                .bodyToMono(DigitalCheck.class)
                .block();
    }
}
